﻿using System;
using Delivery.BL;

namespace Delivery.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.Write("Enter Weight in kg: ");
                var weight = Convert.ToInt16(Console.ReadLine());
                Console.Write("Enter Height in cm: ");
                var height = Convert.ToInt16(Console.ReadLine());
                Console.Write("Enter Width in cm: ");
                var width = Convert.ToInt16(Console.ReadLine());
                Console.Write("Enter depth: ");
                var depth = Convert.ToInt16(Console.ReadLine());
                var calculator = new DeliveryCostCalculator();
                var cost = calculator.CalculateParcelCost(new Parcel(weight, height, width, depth));
                Console.WriteLine($"Cost: ${cost}");
                Console.ReadLine();
                 
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }
            
        }
    }
}
