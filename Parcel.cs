﻿namespace Delivery.BL
{
    public class Parcel
    {
        public int Weight { get; internal set; }
        public int Height { get; internal set; }
        public int Width { get; internal set; }
        public int Depth { get; internal set; }

        public int Volume => Height * Width * Depth;

        public Parcel(int weight, int height, int width, int depth)
        {
            Weight = weight;
            Height = height;
            Width = width;
            Depth = depth;
        }
    }
}
