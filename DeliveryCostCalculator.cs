﻿using System;
using System.Collections.Generic;
using System.Linq;
using Delivery.BL.ParcelRules;

namespace Delivery.BL
{
    public class DeliveryCostCalculator:IDeliveryCalculator
    {
        readonly List<IParcelRule> _rules = new List<IParcelRule>();

        public DeliveryCostCalculator()
        {
            _rules.Add(new RejectParcelRule());
            _rules.Add(new HeavyParcelRule());
            _rules.Add(new SmallParcelRule());
            _rules.Add(new MediumParcelRule());
            _rules.Add(new LargeParcelRule());
        }
        public decimal CalculateParcelCost(Parcel item)
        {
            if (item == null) throw new ArgumentException("Parcel cannot be null.");
            return _rules.Select(rule => rule.CalculateParcelCost(item)).Concat(new decimal[] {0}).Max();
        }
    }
}
