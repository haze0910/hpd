﻿namespace Delivery.BL.ParcelRules
{
    public class SmallParcelRule :IParcelRule
    {
        public decimal CalculateParcelCost(Parcel item)
        {
            if (item.Volume < 1500)
            {
                return (decimal)0.05 * item.Volume;
            }
            return 0;
        }
    }
}
