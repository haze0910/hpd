﻿namespace Delivery.BL.ParcelRules
{
    public class MediumParcelRule : IParcelRule
    {
        public decimal CalculateParcelCost(Parcel item)
        {
            if (item.Volume < 2500 && item.Volume >= 1500)
            {
                return (decimal)0.04 * item.Volume;
            }
            return 0;
        }
    }
}
