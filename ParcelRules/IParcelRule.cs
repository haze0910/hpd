﻿namespace Delivery.BL
{
    public interface IParcelRule
    {
        decimal CalculateParcelCost(Parcel item);
    }
}
