﻿using System;

namespace Delivery.BL
{
    public class RejectParcelRule:IParcelRule
    {
        public decimal CalculateParcelCost(Parcel item)
        {
            if (item.Weight > 50)
            {
                throw new ArgumentException("Weight cannot be more than 50 kilos. Parcel rejected.");
            }
            return 0;
        }
    }
}
