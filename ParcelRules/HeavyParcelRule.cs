﻿namespace Delivery.BL.ParcelRules
{
    public class HeavyParcelRule: IParcelRule
    {
        public decimal CalculateParcelCost(Parcel item)
        {
            if (item.Weight > 10 && item.Weight < 50)
            {
                return 15 * item.Weight;
            }
            return 0;
        }
    }
}
